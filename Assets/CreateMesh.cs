﻿using UnityEngine;
using System.Collections;

[RequireComponent((typeof(MeshFilter)))]
public class CreateMesh : MonoBehaviour {

	public Vector3[] newVertices;
	public Vector2[] newUV;
	public int[] newTriangles;

	Mesh mesh;
	MeshCollider mesh_collider;

	void Start()
	{
		mesh = GetComponent<MeshFilter>().mesh;
		newVertices = mesh.vertices;
		newUV = mesh.uv;
		newTriangles = mesh.triangles;

		mesh_collider = GetComponent<MeshCollider>();
	}

	void Update()
	{
		/*
		mesh.vertices = new Vector3[] 
		{ 
			new Vector3(-0.5f, -0.5f, 0.0f), 
			new Vector3(0.5f, 0.5f, 0.0f), 
			new Vector3(0.5f, -0.5f, 0.0f),
			new Vector3(-0.5f, 0.5f, 0.0f),
			new Vector3(1.0f, 1.0f, 0.0f)
		};
		*/
		mesh.vertices = newVertices;

		mesh.RecalculateNormals();
		//mesh.Optimize();

		mesh_collider.sharedMesh = null;
		mesh_collider.sharedMesh = mesh;
	}
}
