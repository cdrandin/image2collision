Image2Collision
===============

A system which will eventually parse through an image and the main focus is the grey scale value of each pixel.   
From the collection of pixel which are in focus. Construct a mesh that can be wrapped around the group of pixels and is convex.  
From this mesh it will be used for as a mesh collider in which 2D games and seamlessly be able to walk on the key parts of the texture.  

That is the dream.  